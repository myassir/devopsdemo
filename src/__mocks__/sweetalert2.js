const sweetalert = jest.genMockFromModule('sweetalert2');

sweetalert.fire = jest.fn().mockResolvedValue({value: true})

module.exports = sweetalert;