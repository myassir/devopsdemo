import React from 'react';
import { withRouter } from "react-router";
import { Link } from "react-router-dom";
import { Blog } from './State/State';
class ArticleClass extends React.Component{
    constructor(props){
        super()
        this.article = props.article
    }
    componentDidMount(){
        //const {id} = this.props.match.params
    }
    render(){
        return (
            <div className="article-wrapper">
                <div className="article">
                    <div className="img">
                        <img alt={this.article.title} src={this.article.image}></img>
                    </div>
                    <div className="content">
                        <div>
                            <h4>{this.article.title}</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                        <div className="toolbox">
                            <Link to={`/view/${this.article.id}`}>
                                <i className="fa fa-eye"></i>
                            </Link>
                            <Link to={`/article/${this.article.id}`}>
                                <i className="fa fa-edit"></i>
                            </Link>
                            <Link to="#" onClick={() => {Blog.deleteArticle(this.article.id)}}>
                                <i className="fa fa-trash-alt"></i>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export const Article = withRouter(ArticleClass);