import React from 'react';
import './App.css';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import { Home } from './Home/Home';
import { ArticlePage } from './ArticlePage/ArticlePage';
import { PreviewArticle } from './PreviewArticle/PreviewArticle';

export class App extends React.Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route path="/article/:id" children={<ArticlePage />} />
                    <Route path="/view/:id" children={<PreviewArticle />} />
                    <Route path="/">
                        <Home />
                    </Route>
                </Switch>
            </Router>
        );
    }
}

export default App;
