import React from 'react';
import './style.css';
import DanteEditor from "Dante2";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";
import { Blog } from '../State/State';

export class ArticlePageClass extends React.Component{
    constructor(props){
        super()      
        this.article = Object.create(Blog.Articles.find(o => o.id === Number.parseInt(props.match.params.id,10)))
    }
    deleteArticle(){
        Blog.deleteArticle(this.article.id)
        this.props.history.push('/');
    }
    saveArticle(){
        Blog.saveArticle(this.article)
    }
    render(){
        return(
            <div>
                <div className="header">
                    <div className="title">
                        <Link to="/">
                            <i className="fa fa-arrow-left"></i>
                        </Link>
                        <div onInput={(e) => this.article.title = e.target.textContent} contentEditable={true}>
                            {this.article.title} 
                        </div>
                    </div>
                    <div>
                        <button onClick={() => this.deleteArticle()} className="btn btn-danger">
                            Delete
                            <i className="fa fa-trash"></i>
                        </button>
                        <button onClick={() => this.saveArticle()} className="btn btn-primary">
                            Save
                            <i className="fa fa-save"></i>
                        </button>
                    </div>
                </div>
                <div className="editor-container">
                    <div className="editor">
                        <img alt={this.article.title} src={this.article.image}></img>
                        <h1>{this.article.title}</h1>
                        <DanteEditor 
                            onChange={editor => { this.article.data = editor.emitSerializedOutput() }}
                            content={this.article.data} />
                    </div>
                </div>
            </div>
        )
    }
}

export const ArticlePage = withRouter(ArticlePageClass);