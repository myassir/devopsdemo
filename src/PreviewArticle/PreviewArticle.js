import React from 'react';
import { Link } from "react-router-dom";
import DanteEditor from "Dante2";
import { Blog } from '../State/State';
import { withRouter } from "react-router";

class PreviewArticleClass extends React.Component{
    constructor(props){
        super()
        this.article = Object.create(Blog.Articles.find(o => o.id === Number.parseInt(props.match.params.id,10)))
    }
    render(){
        return (
            <div>
                <div className="header">
                    <div>
                        <Link className="mr-2" to="/">
                                <i className="fa fa-arrow-left"></i>
                        </Link>
                        <div className="d-inline">
                            <h3 className="d-inline">Devops Blog</h3>
                        </div>
                    </div>
                </div>
                <div className="editor-container">
                    <div className="editor">
                        <img alt={this.article.title} src={this.article.image}></img>
                        <h1>{this.article.title}</h1>
                        <DanteEditor 
                            read_only={true}
                            content={this.article.data} />
                    </div>
                </div>
            </div>
        )
    }
}

export const PreviewArticle = withRouter(PreviewArticleClass);