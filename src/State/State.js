import Swal from 'sweetalert2'

export let Blog = {
    observers : [],
    Articles: [
        {
            id: 1,
            title: "Title Title Title 1",
            image: "https://images.unsplash.com/photo-1541516160071-4bb0c5af65ba?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&h=500&q=80"
        },
        {
            id: 2,
            title: "Title Title Title 2",
            image: "https://images.unsplash.com/photo-1451187580459-43490279c0fa?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&h=500&q=80"
        },
        {
            id: 3,
            title: "Title Title Title 3",
            image: "https://images.unsplash.com/photo-1541516160071-4bb0c5af65ba?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&h=500&q=80"
        },
        {
            id: 4,
            title: "Title Title Title 4",
            image: "https://images.unsplash.com/photo-1541516160071-4bb0c5af65ba?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&h=500&q=80"
        },
        {
            id: 5,
            title: "Title Title Title 5",
            image: "https://images.unsplash.com/photo-1541516160071-4bb0c5af65ba?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&h=500&q=80"
        }
    ],
    saveArticle: function (article) {
        const index = Blog.Articles.findIndex(o => o.id === Number.parseInt(article.id,10))
        Blog.Articles[index].title = article.title;
        Blog.Articles[index].data = article.data;
        localStorage.setItem("articles", JSON.stringify(this.Articles))
    },
    addArticle: function(title, url){
        this.Articles.push({
            id: Math.max.apply(Math, this.Articles.map(function(o) { return o.id; })) + 1,
            title: title,
            image: url
        })
        localStorage.setItem("articles", JSON.stringify(this.Articles))
        for(let observer of this.observers) {            
            observer.forceUpdate();
        };
    },
    deleteArticle: function (id) {
        return Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.value) {
                this.Articles.splice(Blog.Articles.findIndex(o => o.id === id), 1);                
                localStorage.setItem("articles", JSON.stringify(this.Articles))
                for(let observer of this.observers) {            
                    observer.forceUpdate();
                };
            }
        })     
    },
    subscribe: function(object){
        this.observers.push(object)
    },
    unsubscribe: function(object){
        this.observers.splice(this.observers.findIndex(o => o === object), 1);
    },
}

export function loadBlog(){
    let articles = JSON.parse(localStorage.getItem("articles"));
    Blog.Articles = articles;
}