// import React from 'react';
// import { render } from '@testing-library/react';
// import App from './App';
import { Blog } from './State';

jest.mock('sweetalert2');


test('Blog should have more articles when add', () => {
    let length = Blog.Articles.length
    Blog.addArticle("Title", "https://images.unsplash.com/")
    expect(Blog.Articles.length - length).toBe(1)
});

test('Ensure that article is deleted', async () => {
    let length = Blog.Articles.length
    let id = Blog.Articles.find(() => true).id
    
    await Blog.deleteArticle(id)
    expect(length - Blog.Articles.length).toBe(1);
    expect(Blog.Articles.findIndex(o => o.id === id)).toBe(-1);
});
