import React from 'react';
import { render } from '@testing-library/react';
import { Home } from './Home';
import { Blog } from '../State/State';
import { MemoryRouter } from 'react-router-dom';

test('Home page should show all articles', () => {
    let {getByText} = render(
            <MemoryRouter>
            <Home/>
            </MemoryRouter>
        )
    for(let article of Blog.Articles){
        const element = getByText(article.title);
        expect(element).toBeInTheDocument();
    }
});