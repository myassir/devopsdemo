import React from 'react';
import { Article } from '../Article';
import { Blog } from '../State/State';
import Swal from 'sweetalert2'

import './Home.css';

export class Home extends React.Component{
    componentDidMount(){
        Blog.subscribe(this)
    }
    componentWillUnmount(){
        Blog.unsubscribe(this)
    }
    newArticle(){
        Swal.fire({
            title: 'Add new article',
            html:
              '<input id="swal-input1" placeholder="Title" class="swal2-input">' +
              '<input id="swal-input2" placeholder="Image URL" class="swal2-input">',
            focusConfirm: true,
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonText: 'Save',
            width: '700px',
            showCloseButton: true,
            preConfirm: () => {
                const title = document.getElementById('swal-input1').value
                const url = document.getElementById('swal-input2').value
                Blog.addArticle(title, url)
            }
        })
    }
    render(){
        let atricles = [];
        for(let article of Blog.Articles) {
            atricles.push(<Article key={article.id} article={article} />)
        }
        return (
            <div>
                <div className="header">
                    <h3>Devops Blog</h3>
                    <button onClick={() => this.newArticle()} className="btn btn-dark">
                        New
                        <i className="fa fa-plus-square"></i>
                    </button>
                </div>
                <div className="articles">
                    <div className="container" style={{display: "flex", flexWrap: "wrap"}}>
                        {atricles}
                    </div>
                </div>
            </div>
        )
    }
}